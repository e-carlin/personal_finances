from distutils.core import setup

setup(
    name='personal_finances',
    author='e-carlin',
    author_email='evan@carlin.com',
    version='0.0.1',
    packages=['backend',],
    license='GPLv3',
    install_requires=[
        'tornado'
    ],
)