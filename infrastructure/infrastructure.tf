provider "aws" {
  profile    = "default"
  region     = "us-west-2"
}

resource "aws_s3_bucket" "e98vaxrb521e5-personal-finances-sam" {
  bucket = "e98vaxrb521e5-personal-finances-sam"
  acl    = "private"
}
