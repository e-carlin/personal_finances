import json
import os
import plaid

PLAID_CLIENT_ID = os.environ['PLAID_CLIENT_ID']
PLAID_ENV = os.environ['PLAID_ENV']
PLAID_PUBLIC_KEY = os.environ['PLAID_PUBLIC_KEY']
PLAID_SECRET = os.environ['PLAID_SECRET']


def lambda_handler(event, context):
    res = {
        'statusCode': 500,
        'headers': {
            'Content-Type': 'application/json',
            # TODO(e-carlin): Find a way to standardize across all functions
            # also restrict.
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Headers': '*',
            'Access-Control-Allow-Methods': '*'
        },
    }
    d = json.loads(event['body'])
    public_token = d['publicToken']
    try:
        client = plaid.Client(client_id=PLAID_CLIENT_ID, secret=PLAID_SECRET,
                              public_key=PLAID_PUBLIC_KEY, environment=PLAID_ENV, api_version='2019-05-29')
        exchange_response = client.Item.public_token.exchange(public_token)
        print(exchange_response)
    except plaid.errors.PlaidError as e:
        res.update(
            {
                'body': json.dumps({
                    'error': str(e),
                })
            }
        )
        return res
    access_token = exchange_response['access_token']
    print(access_token)
    res.update(
        {
            'body': json.dumps({
                'message': 'bank-auth',
            })
        }
    )
    return res
