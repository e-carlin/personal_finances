# personal_finances app

This app will record all of my personal finance comings and goings. I want to notify
myself of each expenditure so I better track my finances and watch out for fraud.

## Running locally

This app uses sam-local to run locally. Due to a bug in sam-local it can not
watch for changes in projects with python dependencies. So use the command below
to watch for changes and re-run the build

`watchmedo shell-command bank_auth/ -c='sam build' -p='*.py' --wait -W # Watches for changes`
`sam local start-api --env-vars sandbox-env.json # optional --debug`
