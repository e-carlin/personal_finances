const postPlaidPublicToken = async (publicToken, metadata) => {
    await postFetch('bank-auth', { publicToken, metadata });
};

const postFetch = async (path, data) => {
    console.log('*************************');
    console.log(JSON.stringify(data));
    console.log('*************************');
    const res = await fetch(`http://localhost:3000/${path}`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data), // body data type must match "Content-Type" header
    });
    if (!res.ok) {
        throw new Error(`!res.ok. res=${res}`);
    }
    return await res.json();
};

export { postPlaidPublicToken };
