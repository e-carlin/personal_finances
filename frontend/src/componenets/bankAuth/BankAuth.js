import React, { useState } from 'react';
import Script from 'react-load-script';
import { postPlaidPublicToken } from '../../requestService';

const PLAID_URL = 'https://cdn.plaid.com/link/v2/stable/link-initialize.js';

function BankAuth() {
    const [linkHandler, setlinkHandler] = useState();
    const openLink = () => {
        linkHandler.open();
    };

    const onScriptError = () => {
        console.error('Error loading plaid script');
    };

    const onLinkHandlerSuccess = (publicToken, metaData) => {
        postPlaidPublicToken(publicToken, metaData)
            .then(() => console.log('SUCCESS'))
            .catch(e => console.log(`error=${e}`));
    };

    const onScriptLoaded = () => {
        setlinkHandler(
            window.Plaid.create({
                apiVersion: 'v2',
                clientName: 'Evan',
                env: 'sandbox',
                product: ['transactions'],
                countryCodes: ['US'],
                key: '4d466493d9d1750a67a697b5262c69', // TODO(e-carlin): Move to env var
                language: 'en',
                onSuccess: onLinkHandlerSuccess,
            }),
        );
    };

    return (
        <div>
            <main>
                <div>
                    <div>
                        <h1>Authorize a connection with your bank</h1>
                        <button onClick={openLink}>Connect</button>
                        <button
                            onClick={() =>
                                onLinkHandlerSuccess('abc', { foo: 'bar' })
                            }>
                            Test
                        </button>
                        <Script
                            url={PLAID_URL}
                            onError={onScriptError}
                            onLoad={onScriptLoaded}
                        />
                    </div>
                </div>
            </main>
        </div>
    );
}

export default BankAuth;
