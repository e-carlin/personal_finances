module.exports = {
  plugins: ['prettier', 'react'],
  rules: {
    'prettier/prettier': 'error',
    eqeqeq: 'warn',
  },
  env: {
    es6: true,
  },
  parserOptions: {
    sourceType: 'module',
  },
};
